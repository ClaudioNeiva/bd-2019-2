﻿delete from veiculo_acessorio ;
delete from veiculo ;
delete from acessorio ;
delete from modelo ;
delete from fabricante ;
delete from tabela_precos ;
delete from grupo ;

insert into grupo (nm) 
values 
 ('Básico')
,('Intermediário') ;

insert into tabela_precos (cd_grupo, dt_inicio_vigencia, vl_diaria) 
values 
 ((select cd from grupo where nm = 'Básico'), '2019-06-01', 95.99)
,((select cd from grupo where nm = 'Básico'), '2019-10-01', 135.99);

insert into fabricante (nu_cnpj,nm,nu_telefone,ds_endereco)
values 
('99867977000178','VW','11456765432',('Rua da vw','123','Sala 101','São Paulo','São Paulo')),
('69582840000106','GM','11456766177',('Rua da gm','345','Andar 1','São Paulo','São Paulo')),
('32984862000192','Fiat','61456761231',('Rua da Fiat','789','Sala 202','Belo Horizonte','Minas Gerais'))
;

insert into modelo (nm, nu_cnpj_fabricante) 
values 
('Polo','99867977000178'),
('Virtus','99867977000178'),
('Argo','32984862000192'),
('Siena','32984862000192')
;

insert into acessorio (sg, nm)
values
('VE','Vidro elétrico'),
('TE','Trava elétrica'),
('DH','Direção hidráulica'),
('AR','Ar condicionado'),
('RB','Reboque')
;

insert into veiculo (nu_placa,nu_chassi,cd_modelo,cd_grupo,vl_ano_fabricacao,dt_aquisicao,dt_venda,vl_km)
values
('ABC1234','75645342534567891',(select cd from modelo where nm='Polo'),(select cd from grupo where nm='Intermediário'),2017,'2017-06-12','2018-01-05',30000),
('XYZ1A34','54654656546546546',(select cd from modelo where nm='Virtus'),(select cd from grupo where nm='Intermediário'),2017,'2017-08-20','2018-01-05',25000),
('PLM3D34','56756756777566767',(select cd from modelo where nm='Virtus'),(select cd from grupo where nm='Básico'),2018,'2018-05-01',null,15000),
('ABC4567','54656756756756612',(select cd from modelo where nm='Argo'),(select cd from grupo where nm='Intermediário'),2017,'2017-08-20',null,28000),
('PLN2354','78356456463223423',(select cd from modelo where nm='Argo'),(select cd from grupo where nm='Básico'),2019,'2019-06-01',null,7000),
('JKL2131','45654678785468568',(select cd from modelo where nm='Argo'),(select cd from grupo where nm='Básico'),2019,'2019-06-01',null,9000)
;

insert into veiculo_acessorio (nu_placa, sg_acessorio)
values 
('ABC1234', 'VE'),
('ABC1234', 'TE'),
('ABC1234', 'AR'),
('ABC1234', 'DH'),
('PLM3D34', 'VE'),
('PLM3D34', 'TE'),
('ABC4567', 'VE'),
('ABC4567', 'TE'),
('ABC4567', 'AR'),
('JKL2131', 'VE'),
('JKL2131', 'TE')
;

nu_placa   count(*)	
'ABC1234', 4
'PLM3D34', 2
'ABC4567', 3
'JKL2131', 2

-- As placas dos veículos que possuem mais de 2 acessórios;
-- Saída: nu_placa.
select 	nu_placa
from 	veiculo_acessorio
group by nu_placa
having 	count(*) > 2
;

-- Veículos por ordem crescente de cd_grupo e, dentro do código de grupo, por ordem de cd_modelo, e, 
-- dentro de código de modelo, por nu_placa, este último em ordem decrescente;
-- Saída: cd_grupo, cd_modelo, nu_placa, vl_ano_fabricacao e vl_km.
select 	cd_grupo, 
	cd_modelo, 
	nu_placa, 
	vl_ano_fabricacao,
	vl_km
from 	veiculo	
order by cd_grupo asc,
	 cd_modelo asc, 
	 nu_placa desc;

-- Todos os modelos combinados com todos os fabricantes;
-- Saída: cd e nm do modelo e nu_cnpj, nm e nm_estado do fabricante.
select	m.cd as "codigo_modelo",
	m.nm as "nome_modelo",
	f.nu_cnpj as "nu_cnpj_fabricante",
	f.nm as "nome_fabricante",
	(f.ds_endereco).nm_estado as "nome_estado_fabricante"
from	modelo m,
	fabricante f;

select	m.cd as "codigo_modelo",
	m.nm as "nome_modelo",
	f.nu_cnpj as "nu_cnpj_fabricante",
	f.nm as "nome_fabricante",
	(f.ds_endereco).nm_estado as "nome_estado_fabricante"
from	modelo m
cross join fabricante f;

-- Todos os modelos combinados com todos os fabricantes, filtrando para os fabricantes que fabricaram os modelos;
-- Saída: cd e nm do modelo e nu_cnpj, nm e nm_estado do fabricante.
-- NÃO FAÇA PRODUTO CARTESIANO ONDE VOCÊ DESEJA UMA JUNÇÃO, COMO NESTE EXEMPLO.
-- NÃO FAÇA ASSIM!!!!
select	m.cd as "codigo_modelo",
	m.nm as "nome_modelo",
	f.nu_cnpj as "nu_cnpj_fabricante",
	f.nm as "nome_fabricante",
	(f.ds_endereco).nm_estado as "nome_estado_fabricante"
from	modelo m,
	fabricante f
where	m.nu_cnpj_fabricante = f.nu_cnpj;

select	m.cd as "codigo_modelo",
	m.nm as "nome_modelo",
	f.nu_cnpj as "nu_cnpj_fabricante",
	f.nm as "nome_fabricante",
	(f.ds_endereco).nm_estado as "nome_estado_fabricante"
from	modelo m
cross join fabricante f
where	m.nu_cnpj_fabricante = f.nu_cnpj;

-- Os modelos e seus respectivos fabricantes;
-- Saída: cd e nm do modelo e nu_cnpj, nm e nm_estado do fabricante.
select	m.cd as "codigo_modelo",
	m.nm as "nome_modelo",
	f.nu_cnpj as "nu_cnpj_fabricante",
	f.nm as "nome_fabricante",
	(f.ds_endereco).nm_estado as "nome_estado_fabricante"
from	modelo m
inner join fabricante f on (f.nu_cnpj = m.nu_cnpj_fabricante);

-- Os modelos e seus respectivos fabricantes;
-- Filtro: apenas fabricantes do estado de São Paulo;
-- Saída: cd e nm do modelo e nu_cnpj, nm e nm_estado do fabricante.
select	m.cd as "codigo_modelo",
	m.nm as "nome_modelo",
	f.nu_cnpj as "nu_cnpj_fabricante",
	f.nm as "nome_fabricante",
	(f.ds_endereco).nm_estado as "nome_estado_fabricante"
from	modelo m
inner join fabricante f on (f.nu_cnpj = m.nu_cnpj_fabricante)
where	(f.ds_endereco).nm_estado = 'São Paulo';

-- Todo os fabricantes, e os seus modelos, quando existirem. Fabricantes que não possuem modelos, 
-- no lugar do nome do modelo deve ser exibida a mensagem "(Nenhum modelo para este fabricante)";
-- Filtro: apenas fabricantes do estado de São Paulo;
-- Saída: cd e nm do modelo e nu_cnpj, nm e nm_estado do fabricante.
select	m.cd as "codigo_modelo",
	coalesce(m.nm, '(Nenhum modelo para este fabricante)') as "nome_modelo",
	f.nu_cnpj as "nu_cnpj_fabricante",
	f.nm as "nome_fabricante",
	(f.ds_endereco).nm_estado as "nome_estado_fabricante"
from	modelo m
right outer join fabricante f on (f.nu_cnpj = m.nu_cnpj_fabricante)
where	(f.ds_endereco).nm_estado = 'São Paulo';

select	m.cd as "codigo_modelo",
	coalesce(m.nm, '(Nenhum modelo para este fabricante)') as "nome_modelo",
	f.nu_cnpj as "nu_cnpj_fabricante",
	f.nm as "nome_fabricante",
	(f.ds_endereco).nm_estado as "nome_estado_fabricante"
from	fabricante f
left outer join modelo m on (m.nu_cnpj_fabricante = f.nu_cnpj)
where	(f.ds_endereco).nm_estado = 'São Paulo';

-- Todos os veículos com seus respectivos acessorios. Veículos sem acessório (descrição do acessório
-- '(Nenhum acessório)')e acessórios que não estão instalados em nenhum veículo (descrição do veículo 
-- '(Nenhum veículo)';
-- Saída: nu_placa e nm_acessorio;
-- Filtro: sem filtro.
select  coalesce(v.nu_placa, '(Nenhum veículo)') as "nu_placa_veiculo",
	coalesce(a.nm,'(Nenhum acessório)') as "nm_acessorio"
from 	veiculo v
left outer join veiculo_acessorio va on (va.nu_placa  = v.nu_placa)
full outer join acessorio a on (a.sg = va.sg_acessorio);

-- Listar todas as pessoas (empregados e dependentes, sem associá-los), e para cada pessoa o seu telefone.
-- Saída: nm e nu_telefone;
-- Ordenação: nm ascendente.
drop table if exists empregado cascade;
drop table if exists dependente cascade;

create table empregado (
nu_cpf		char(11) 	not null,
nm		varchar(40)	not null,
nu_telefone 	char(11)	not null,
constraint pk_empregado
	primary key (nu_cpf));
	
create table dependente (
nu_cpf_empregado	char(11) 	not null,
nu_cpf			char(11)	not null,	
nm			varchar(40)	not null,
nu_telefone 		char(11)	not null,
constraint pk_dependente
	primary key (nu_cpf_empregado, nu_cpf));	

alter table dependente 
	add constraint fk_dependente_empregado
		foreign key (nu_cpf_empregado)
		references empregado
		on delete cascade
		on update cascade;

insert into empregado (nu_cpf, nm, nu_telefone)
values
('123','Antonio','5467567'),
('456','Claudio','8378475'),
('678','Maria'  ,'2646565'),
('890','Ana'	,'5756722')
;
insert into dependente (nu_cpf_empregado, nu_cpf, nm, nu_telefone)
values
('123','642','Joaquim','8547823'),
('123','964','Clara'  ,'4754672'),
('678','098','Joana'  ,'3471223'),
('890','023','Samuel' ,'8676779'),
('890','678','Maria'  ,'2646565')
;

-- Union faz um distinct automático. Pra este caso, UNION faz mais sentido que UNION ALL
select	e.nm,
	e.nu_telefone
from 	empregado e
union
select	d.nm,
	d.nu_telefone
from 	dependente d
order by 1 asc;

-- Union all NÃO faz distinct automático
select	e.nm,
	e.nu_telefone
from 	empregado e
union all
select	d.nm,
	d.nu_telefone
from 	dependente d
order by 1 asc;

-- Todos os veículos que não possuem nenhum acessório.
-- Saída: nu_placa e o nm do modelo.
select	v.nu_placa,
	m.nm
from	veiculo v
inner join modelo m on (m.cd = v.cd_modelo)
where 	v.nu_placa not in (select distinct va.nu_placa from veiculo_acessorio va);

-- NUNCA FAÇA ASSIM!!!! Existe um grande peder de performance quando a consulta interna faz junção com a 
-- consulta externa.
select	v.nu_placa,
	m.nm
from	veiculo v
inner join modelo m on (m.cd = v.cd_modelo)
where 	not exists (select * from veiculo_acessorio va where va.nu_placa = v.nu_placa);

-- Todos os veículos que possuem pelo menos um acessório.
-- Saída: nu_placa e o nm do modelo.
select	v.nu_placa,
	m.nm
from	veiculo v
inner join modelo m on (m.cd = v.cd_modelo)
where 	v.nu_placa in (select distinct va.nu_placa from veiculo_acessorio va);

-- NUNCA FAÇA ASSIM!!!! Existe um grande peder de performance quando a consulta interna faz junção com a 
-- consulta externa.
select	v.nu_placa,
	m.nm
from	veiculo v
inner join modelo m on (m.cd = v.cd_modelo)
where 	exists (select * from veiculo_acessorio va where va.nu_placa = v.nu_placa);

-- in e not in podem ser utilizados para criar listas de dados fixos
-- Todos os dados dos veículos de placa ABC1234, PLM3D34 e ABC4567
select 	*
from 	veiculo
where   nu_placa = 'ABC1234' 
or	nu_placa = 'PLM3D34' 
or	nu_placa = 'ABC4567';
-- Melhor assim:
select 	*
from 	veiculo
where   nu_placa in ('ABC1234', 'PLM3D34','ABC4567');


-- Criar uma tabela com os nomes de modelo e respectivas placas de veículo.
-- Filtro: apenas modelos da VW
select	m.nm as "nm_modelo",
	v.nu_placa as "nu_placa_veiculo"
into	veiculos_vw	
from 	veiculo v
inner join modelo m on (m.cd = v.cd_modelo)
inner join fabricante f on (f.nu_cnpj = m.nu_cnpj_fabricante)
where	f.nm = 'VW';	
-- A tabela veiculos_vw foi criada automaticamente pela consulta anterior.
select 	* 
from 	veiculos_vw;
-- Para executar a consulta que gerou a tabela, antes você deve excluir a veiculos_vw.
