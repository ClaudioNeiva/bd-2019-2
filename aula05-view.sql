﻿drop view if exists v_veiculo;

create or replace view v_veiculo 
as
select  v.nu_placa	as "nu_placa",
	v.nu_chassi 	as "nu_chassi",
	v.cd_modelo 	as "cd_modelo",
	v.cd_grupo 	as "cd_grupo",
	m.nm 		as "nm_modelo",
	g.nm		as "nm_grupo",
	f.nu_cnpj	as "nu_cnpj_fabricante",
	f.nm		as "nm_fabricante"
from veiculo v
inner join modelo m on (m.cd = v.cd_modelo)
inner join grupo g on (g.cd = v.cd_grupo)
inner join fabricante f on (f.nu_cnpj = m.nu_cnpj_fabricante)
where	v.dt_venda is null
;

select	*
from 	v_veiculo v
inner join veiculo_acessorio va on (va.nu_placa = v.nu_placa)
where	nm_modelo ilike 'polo';

drop materialized view if exists v_veiculo_mat;

create materialized view v_veiculo_mat
as
select  v.nu_placa	as "nu_placa",
	v.nu_chassi 	as "nu_chassi",
	v.cd_modelo 	as "cd_modelo",
	v.cd_grupo 	as "cd_grupo",
	m.nm 		as "nm_modelo",
	g.nm		as "nm_grupo",
	f.nu_cnpj	as "nu_cnpj_fabricante",
	f.nm		as "nm_fabricante"
from veiculo v
inner join modelo m on (m.cd = v.cd_modelo)
inner join grupo g on (g.cd = v.cd_grupo)
inner join fabricante f on (f.nu_cnpj = m.nu_cnpj_fabricante)
where	v.dt_venda is null
;

select	count(*)
from 	v_veiculo_mat v;

insert into veiculo (nu_placa,nu_chassi,cd_modelo,cd_grupo,vl_ano_fabricacao,dt_aquisicao,dt_venda,vl_km)
values
('KHJ5232','78434789789347893',(select cd from modelo where nm='Virtus'),(select cd from grupo where nm='Intermediário'),2019,'2019-05-01',null,2000),
('LOP7755','78904789356781359',(select cd from modelo where nm='Virtus'),(select cd from grupo where nm='Intermediário'),2019,'2019-05-01',null,1000),
('GTY1235','54uioeuioe0908588',(select cd from modelo where nm='Polo'),(select cd from grupo where nm='Intermediário'),2018,'2018-07-12',null,12000),
('ABU1A36','93485671785756767',(select cd from modelo where nm='Virtus'),(select cd from grupo where nm='Intermediário'),2018,'2018-08-20',null,19000),
('ABC1235','13463454563423445',(select cd from modelo where nm='Polo'),(select cd from grupo where nm='Intermediário'),2017,'2017-07-12','2018-01-08',32000),
('XYZ1A36','897783y7667789782',(select cd from modelo where nm='Virtus'),(select cd from grupo where nm='Intermediário'),2017,'2016-08-20','2018-01-08',29000)
;

select	*
from 	v_veiculo_mat v;

select	count(*) 
from 	veiculo;

select	count(*)
from 	v_veiculo v;

select	count(*)
from 	v_veiculo_mat v;

refresh materialized view v_veiculo_mat;

select	count(*)
from 	v_veiculo_mat v;

