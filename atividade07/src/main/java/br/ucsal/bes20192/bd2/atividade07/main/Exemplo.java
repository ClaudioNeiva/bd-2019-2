package br.ucsal.bes20192.bd2.atividade07.main;

import java.util.ArrayList;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import br.ucsal.bes20192.bd2.atividade07.domain.Modelo;
import br.ucsal.bes20192.bd2.atividade07.domain.Pessoa;
import br.ucsal.bes20192.bd2.atividade07.domain.Veiculo;
import br.ucsal.bes20192.bd2.atividade07.dto.QuantidadeVeiculosModeloDTO;
import br.ucsal.bes20192.bd2.atividade07.enums.TipoVeiculoEnum;

public class Exemplo {

	private static EntityManagerFactory emf;

	private static EntityManager em;

	public static void main(String[] args) {

		emf = Persistence.createEntityManagerFactory("atividade07");
		em = emf.createEntityManager();

		popularBase();

		consultarCompradoresVeiculo("Gol");

		consultarQtdVeiculosModelo(TipoVeiculoEnum.PASSEIO);

		consultarQtdVeiculosModeloUsandoVetorObjetos(TipoVeiculoEnum.PASSEIO);

		consultarVeiculosCompradorModelo("Claudio Neiva", "Gol");

		em.close();

		emf.close();
	}

	private static void consultarCompradoresVeiculo(String nomeModelo) {
		// Limpar o EntityManager � opcional e est� sendo utilizado apenas para
		// acompanhamento de todas as consultas SQL consequentes �s consultas JPQL
		// executadas.
		em.clear();

		String oql = "select v.comprador from Veiculo v where v.modelo.nome = :nomeModelo";
		TypedQuery<Pessoa> query = em.createQuery(oql, Pessoa.class);
		query.setParameter("nomeModelo", nomeModelo);
		System.out.println("Compradores de ve�culos: modelo " + nomeModelo);
		for (Pessoa pessoa : query.getResultList()) {
			System.out.println(pessoa);
		}
	}

	private static void consultarQtdVeiculosModelo(TipoVeiculoEnum tipoVeiculo) {
		// Limpar o EntityManager � opcional e est� sendo utilizado apenas para
		// acompanhamento de todas as consultas SQL consequentes �s consultas JPQL
		// executadas.
		em.clear();

		String oql = "select new " + QuantidadeVeiculosModeloDTO.class.getName()
				+ "(v.modelo.nome, count(*)) from Veiculo v where v.tipo = :tipoVeiculo group by v.modelo.nome";
		TypedQuery<QuantidadeVeiculosModeloDTO> query = em.createQuery(oql, QuantidadeVeiculosModeloDTO.class);
		query.setParameter("tipoVeiculo", tipoVeiculo);
		System.out.println("Quantidade de ve�culos por modelo: tipo de ve�culo " + tipoVeiculo + " (usando DTO)");
		for (QuantidadeVeiculosModeloDTO quantidadeVeiculosModeloDTO : query.getResultList()) {
			System.out.println(quantidadeVeiculosModeloDTO);
		}
	}

	private static void consultarQtdVeiculosModeloUsandoVetorObjetos(TipoVeiculoEnum tipoVeiculo) {
		// Limpar o EntityManager � opcional e est� sendo utilizado apenas para
		// acompanhamento de todas as consultas SQL consequentes �s consultas JPQL
		// executadas.
		em.clear();

		String oql = "select v.modelo.nome, count(*) from Veiculo v where v.tipo = :tipoVeiculo group by v.modelo.nome";
		TypedQuery<Object[]> query = em.createQuery(oql, Object[].class);
		query.setParameter("tipoVeiculo", tipoVeiculo);
		System.out.println("Quantidade de ve�culos por modelo: tipo de ve�culo " + tipoVeiculo + " (usando vetor)");
		for (Object[] linha : query.getResultList()) {
			System.out.println(linha[0] + " | " + linha[1]);
		}
	}

	private static void consultarVeiculosCompradorModelo(String nomeComprador, String nomeModelo) {
		// Limpar o EntityManager � opcional e est� sendo utilizado apenas para
		// acompanhamento de todas as consultas SQL consequentes �s consultas JPQL
		// executadas.
		em.clear();

		String oql = "select v from Veiculo v where v.comprador.nome = :nomeComprador and v.modelo.nome = :nomeModelo";
		TypedQuery<Veiculo> query = em.createQuery(oql, Veiculo.class);
		query.setParameter("nomeComprador", nomeComprador);
		query.setParameter("nomeModelo", nomeModelo);
		System.out.println("Ve�culos do comprador " + nomeComprador + ": modelo " + nomeModelo);
		for (Veiculo veiculo : query.getResultList()) {
			System.out.println(veiculo);
		}
	}

	private static void popularBase() {

		Modelo modelo1 = new Modelo("Gol");
		Pessoa comprador1 = new Pessoa("123", "Claudio Neiva", "claudio@neiva.com.br", new ArrayList<>());
		Veiculo veiculo1 = new Veiculo("ABC1234", "1231231232312", modelo1, TipoVeiculoEnum.PASSEIO, new Date(),
				comprador1);

		em.getTransaction().begin();
		em.persist(modelo1);
		em.persist(comprador1);
		em.persist(veiculo1);
		em.getTransaction().commit();

	}

}
