package br.ucsal.bes20192.bd2.atividade07.converters;

import javax.persistence.AttributeConverter;

import br.ucsal.bes20192.bd2.atividade07.enums.TipoVeiculoEnum;

//@Converter(autoApply = true)
public class TipoVeiculoConverter implements AttributeConverter<TipoVeiculoEnum, String> {

	public String convertToDatabaseColumn(TipoVeiculoEnum attribute) {
		return attribute == null ? null : attribute.getCodigo();
	}

	public TipoVeiculoEnum convertToEntityAttribute(String dbData) {
		return dbData == null ? null : TipoVeiculoEnum.valueOfCodigo(dbData);
	}

}
