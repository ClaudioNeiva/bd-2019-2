package br.ucsal.bes20192.bd2.atividade07.dto;

public class QuantidadeVeiculosModeloDTO {

	private String nomeModelo;

	private Long qtdVeiculos;

	public QuantidadeVeiculosModeloDTO(String nomeModelo, Long qtdVeiculos) {
		super();
		this.nomeModelo = nomeModelo;
		this.qtdVeiculos = qtdVeiculos;
	}

	public String getNomeModelo() {
		return nomeModelo;
	}

	public void setNomeModelo(String nomeModelo) {
		this.nomeModelo = nomeModelo;
	}

	public Long getQtdVeiculos() {
		return qtdVeiculos;
	}

	public void setQtdVeiculos(Long qtdVeiculos) {
		this.qtdVeiculos = qtdVeiculos;
	}

	@Override
	public String toString() {
		return "QuantidadeVeiculosModeloDTO [nomeModelo=" + nomeModelo + ", qtdVeiculos=" + qtdVeiculos + "]";
	}

}
