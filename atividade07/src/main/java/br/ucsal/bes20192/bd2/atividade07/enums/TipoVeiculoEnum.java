package br.ucsal.bes20192.bd2.atividade07.enums;

public enum TipoVeiculoEnum {

	CARGA("CR"), PASSEIO("PS");

	private String codigo;

	private TipoVeiculoEnum(String codigo) {
		this.codigo = codigo;
	}

	public static TipoVeiculoEnum valueOfCodigo(String codigo) {
		for (TipoVeiculoEnum tipoVeiculoEnum : values()) {
			if (tipoVeiculoEnum.codigo.equalsIgnoreCase(codigo)) {
				return tipoVeiculoEnum;
			}
		}
		throw new IllegalArgumentException(
				"Codigo " + codigo + " n�o encontrado em " + TipoVeiculoEnum.class.getName());
	}

	public String getCodigo() {
		return codigo;
	}

}
