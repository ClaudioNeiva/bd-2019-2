package br.ucsal.bes20192.bd2.atividade07.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import br.ucsal.bes20192.bd2.atividade07.converters.TipoVeiculoConverter;
import br.ucsal.bes20192.bd2.atividade07.enums.TipoVeiculoEnum;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(name = "uk_veiculo_chassi", columnNames = { "chassis" }) })
public class Veiculo {
	// Observação: o campo chassis deve ser uma unique key.

	@Id
	@Column(columnDefinition = "char(7)")
	private String placa;

	@Column(columnDefinition = "char(17)", nullable = false) // , unique = true)
	private String chassis;

	@ManyToOne(optional = false)
	private Modelo modelo;

	@Convert(converter = TipoVeiculoConverter.class)
	@Column(columnDefinition = "char(2)", nullable = false)
	private TipoVeiculoEnum tipo;

	@Temporal(TemporalType.DATE)
	@Column(name = "data_compra", nullable = false)
	private Date dataCompra;

	@ManyToOne(optional = false)
	private Pessoa comprador;

	public Veiculo() {
		super();
	}

	public Veiculo(String placa, String chassis, Modelo modelo, TipoVeiculoEnum tipo, Date dataCompra,
			Pessoa comprador) {
		super();
		this.placa = placa;
		this.chassis = chassis;
		this.modelo = modelo;
		this.tipo = tipo;
		this.dataCompra = dataCompra;
		this.comprador = comprador;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getChassis() {
		return chassis;
	}

	public void setChassis(String chassis) {
		this.chassis = chassis;
	}

	public Modelo getModelo() {
		return modelo;
	}

	public void setModelo(Modelo modelo) {
		this.modelo = modelo;
	}

	public TipoVeiculoEnum getTipo() {
		return tipo;
	}

	public void setTipo(TipoVeiculoEnum tipo) {
		this.tipo = tipo;
	}

	public Date getDataCompra() {
		return dataCompra;
	}

	public void setDataCompra(Date dataCompra) {
		this.dataCompra = dataCompra;
	}

	public Pessoa getComprador() {
		return comprador;
	}

	public void setComprador(Pessoa comprador) {
		this.comprador = comprador;
	}

	@Override
	public String toString() {
		return "Veiculo [placa=" + placa + ", chassis=" + chassis + ", modelo=" + modelo + ", tipo=" + tipo
				+ ", dataCompra=" + dataCompra + ", comprador=" + comprador.getCpf() + "]";
	}

}
