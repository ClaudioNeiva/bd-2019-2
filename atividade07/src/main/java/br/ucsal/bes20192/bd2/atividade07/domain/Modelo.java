package br.ucsal.bes20192.bd2.atividade07.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "seq_modelo", sequenceName = "sq_modelo")
public class Modelo {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_modelo")
	private Integer codigo;
	
	@Column(length = 50, nullable = false)
	private String nome;

	public Modelo() {
		super();
	}

	public Modelo(String nome) {
		super();
		this.nome = nome;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "Modelo [codigo=" + codigo + ", nome=" + nome + "]";
	}

}
