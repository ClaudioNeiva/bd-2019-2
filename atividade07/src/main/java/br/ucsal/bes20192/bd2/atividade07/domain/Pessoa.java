package br.ucsal.bes20192.bd2.atividade07.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Pessoa {

	@Id
	@Column(columnDefinition = "char(11)")
	private String cpf;

	@Column(length = 40, nullable = false)
	private String nome;

	@Column(length = 250)
	private String email;

	@OneToMany(mappedBy = "comprador")
	private List<Veiculo> veiculos;

	public Pessoa() {
		super();
	}

	public Pessoa(String cpf, String nome, String email, List<Veiculo> veiculos) {
		super();
		this.cpf = cpf;
		this.nome = nome;
		this.email = email;
		this.veiculos = veiculos;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Veiculo> getVeiculos() {
		return veiculos;
	}

	public void setVeiculos(List<Veiculo> veiculos) {
		this.veiculos = veiculos;
	}

	@Override
	public String toString() {
		return "Pessoa [cpf=" + cpf + ", nome=" + nome + ", email=" + email + ", veiculos=" + veiculos + "]";
	}

}
