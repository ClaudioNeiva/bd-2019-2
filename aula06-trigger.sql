﻿-- criando as tabelas de exemplo
drop table if exists conta cascade;
drop table if exists movimento cascade;
drop table if exists saldo cascade;

create table conta (
	cd		serial		not null,
	nm_correntista	varchar(40)	not null,
	constraint pk_conta
		primary key (cd));
		
create table movimento (
	id 		serial		not null,
	cd_conta	integer		not null,
	dt		date		not null,
	vl		numeric(10,2)	not null,
	ds_historico	varchar(100)		,
	constraint pk_movimento
		primary key (id));

create table saldo (
	cd_conta	integer		not null,
	dt		date		not null,
	vl		numeric(10,2)	not null,
	constraint pk_saldo
		primary key (cd_conta, dt));

alter table movimento 
	add constraint fk_movimento_conta
		foreign key (cd_conta)
		references conta;

alter table saldo
	add constraint fk_saldo_conta
		foreign key (cd_conta)
		references conta;

-- criando a função que será disparada pelo trigger

drop function if exists f_trg_ins_upd_movimento();

CREATE OR REPLACE FUNCTION f_trg_ins_upd_movimento()
RETURNS TRIGGER
AS
$$
BEGIN
	RAISE NOTICE 'Valor do movimento %, data do movimento %', NEW.vl, NEW.dt;
	if NEW.vl > 1000 then
		RAISE EXCEPTION 'Não é possível movimentar mais de 1000 reais em uma operação.';
	end if;
	RETURN NEW;
END;
$$
LANGUAGE PLPGSQL;

-- criando o trigger que irá dispara a função para os eventos de insert e update (cd_conta, dt e vl)

drop trigger if exists trg_ins_upd_movimento on movimento;

CREATE TRIGGER trg_ins_upd_movimento
BEFORE INSERT OR UPDATE OF cd_conta, dt, vl
ON movimento
FOR EACH ROW
EXECUTE PROCEDURE f_trg_ins_upd_movimento();

-- preencher a tabela de conta com dados de apoio

insert into conta (nm_correntista)
values 
('Claudio Neiva'),
('Maria da Silva');

-- Alteração da estrutura de trigger para utilização de um trigger único para as três operações.
-- A função de trigger aqui definida funcionam como um trigger "nulo", que não interfere no 
-- funcionamento. É apenas uma estrutura para, adiante, incluirmos a lógica de negócio.
drop trigger if exists trg_ins_upd_movimento on movimento;
drop function if exists f_trg_ins_upd_movimento();

drop trigger if exists trg_ins_upd_del_movimento on movimento;

CREATE OR REPLACE FUNCTION f_trg_ins_upd_del_movimento()
RETURNS TRIGGER
AS
$$
BEGIN
	if TG_OP in ('INSERT', 'UPDATE') then
		-- Aqui vai ficar a lógica de negócio para insert e update
		RETURN NEW;
	else
		-- Aqui vai ficar a lógica de negócio para delete
		RETURN OLD;
	end if;
END;
$$
LANGUAGE PLPGSQL;

drop trigger if exists trg_ins_upd_del_movimento on movimento;

CREATE TRIGGER trg_ins_upd_del_movimento
BEFORE INSERT OR DELETE OR UPDATE OF cd_conta, dt, vl
ON movimento
FOR EACH ROW
EXECUTE PROCEDURE f_trg_ins_upd_del_movimento();

-- Especificação da lógica de negócio para inclusão de movimentos.
CREATE OR REPLACE FUNCTION f_trg_ins_upd_del_movimento()
RETURNS TRIGGER
AS
$$
DECLARE vl_saldo_anterior	numeric;
	dt_saldo_anterior	date;
BEGIN

	if TG_OP IN ('INSERT', 'UPDATE') then

		if TG_OP = 'UPDATE' then
			update	saldo
			set	vl = vl - OLD.vl
			where	cd_conta = OLD.cd_conta
			and	dt >= OLD.dt;
		end if;
			
		if not exists(select * from saldo where cd_conta = NEW.cd_conta and dt = NEW.dt) then
			select	max(dt)
			into	dt_saldo_anterior
			from	saldo
			where	cd_conta = NEW.cd_conta
			and	dt < NEW.dt;
			if dt_saldo_anterior is null then 
				vl_saldo_anterior := 0;
			else
				select	vl
				into	vl_saldo_anterior
				from	saldo
				where	cd_conta = NEW.cd_conta
				and	dt = dt_saldo_anterior;
			end if;

			insert into saldo (cd_conta, dt, vl)
			values (NEW.cd_conta, NEW.dt, vl_saldo_anterior);
		end if;
		
		update	saldo
		set	vl = vl + NEW.vl
		where	cd_conta = NEW.cd_conta
		and	dt >= NEW.dt;

		RETURN NEW;
		
	else
		update	saldo
		set	vl = vl - OLD.vl
		where	cd_conta = OLD.cd_conta
		and	dt >= OLD.dt;
		
		RETURN OLD;
	end if;
END;
$$
LANGUAGE PLPGSQL;

-- Disparo do trigger através da inclusão de um movimento
insert into movimento (cd_conta, dt, vl, ds_historico)
values
((select cd from conta where nm_correntista = 'Maria da Silva'), '2019-08-01', 100, 'Receitas diversas');

insert into movimento (cd_conta, dt, vl, ds_historico)
values
((select cd from conta where nm_correntista = 'Maria da Silva'), '2019-08-01', 500, 'Mesada');

insert into movimento (cd_conta, dt, vl, ds_historico)
values
((select cd from conta where nm_correntista = 'Maria da Silva'), '2019-08-05', -230, 'Conta de luz');

insert into movimento (cd_conta, dt, vl, ds_historico)
values
((select cd from conta where nm_correntista = 'Maria da Silva'), '2019-07-15', 10, 'Depósito');

insert into movimento (cd_conta, dt, vl, ds_historico)
values
((select cd from conta where nm_correntista = 'Maria da Silva'), '2019-09-02', -100, 'Saque');

insert into movimento (cd_conta, dt, vl, ds_historico)
values
((select cd from conta where nm_correntista = 'Maria da Silva'), '2019-10-25', -50 , 'Pagamento de conta');

insert into movimento (cd_conta, dt, vl, ds_historico)
values
((select cd from conta where nm_correntista = 'Maria da Silva'), '2019-08-04', -70 , 'Pagamento de conta');

select 	*
from	movimento
order by dt asc;

select 	*
from	saldo
order by dt asc;

-- teste de remoção de registro de movimento para disparo do trigger e respectiva atualização do saldo
delete
from	movimento
where 	cd_conta = (select cd from conta where nm_correntista = 'Maria da Silva')
and	dt = '2019-08-05';

select 	*
from	movimento
order by dt asc;

select 	*
from	saldo
order by dt asc;

-- teste de atualização de VALOR de registro de movimento para disparo do trigger e respectiva atualização do saldo
update	movimento
set	vl = 400
where 	cd_conta = (select cd from conta where nm_correntista = 'Maria da Silva')
and	dt = '2019-08-01'
and	vl = 500;

select 	*
from	movimento
order by dt asc;

select 	*
from	saldo
order by dt asc;

-- teste de atualização de data e valor de registro de movimento para disparo do trigger e respectiva atualização do saldo
update	movimento
set	vl = 200,
	dt = '2019-08-15'
where 	cd_conta = (select cd from conta where nm_correntista = 'Maria da Silva')
and	dt = '2019-08-04';

select 	*
from	movimento
order by dt asc;

select 	*
from	saldo
order by dt asc;

-- teste de atualização de cd_conta, data e valor de registro de movimento para disparo do trigger e respectiva atualização do saldo
update	movimento
set	cd_conta = (select cd from conta where nm_correntista = 'Claudio Neiva'),
	vl = -60,
	dt = '2019-11-02'
where 	cd_conta = (select cd from conta where nm_correntista = 'Maria da Silva')
and	dt = '2019-07-15';

select 	*
from	movimento
order by dt asc;

select 	*
from	saldo
order by dt asc;
