package br.ucsal.bes20192.bd2.aula08.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Exemplo {

	private static final String SELECT_CORRENTISTA = "select * from correntista order by nm asc";
	private static final String URL = "jdbc:postgresql://localhost:6543/aula08";
	private static final String USER = "postgres";
	private static final String PASSWORD = "postgresql";
	private static Connection connection;
	private static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) throws SQLException {
		conectarSGBD();
		inserirCorrentista();
		inserirCorrentistaRecuperandoIdGeradoSGBD();
		atualizarCorrentistaAPartirCursor();
		inserirCorrentistaAPartirCursor();
		inserirEmLote();
		inserirPassandoParametro();
		consultarCorrentistas();
		consultarCorrentistasTryResource();
		consultarCorrentistasOrdemInvesa();
		descontarSGBD();
	}

	private static void inserirPassandoParametro() throws SQLException {
		System.out.println("\n ************ inserirCorrentistaAPartirCursor ************");
		System.out.println("Informe o nome:");
		String nome = scanner.nextLine();
		String query = "insert into correntista (nm) values (?)";
		try (PreparedStatement stmt = connection.prepareStatement(query)) {
			stmt.setString(1, nome);
			stmt.executeUpdate();
		}
	}

	private static void inserirEmLote() throws SQLException {
		System.out.println("\n ************ inserirEmLote ************");
		connection.setAutoCommit(false);
		try (Statement stmt = connection.createStatement()) {
			stmt.addBatch("insert into correntista (nm) values ('Rui')");
			stmt.addBatch("insert into correntista (nm) values ('Verena')");
			stmt.addBatch("insert into correntista (nm) values ('Jane')");
			stmt.executeBatch();
		}
		connection.commit();
	}

	private static void inserirCorrentistaAPartirCursor() throws SQLException {
		System.out.println("\n ************ inserirCorrentistaAPartirCursor ************");
		String query = "select * from correntista order by nm asc";
		try (Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
				ResultSet rs = stmt.executeQuery(query);) {
			rs.moveToInsertRow();
			rs.updateString("nm", "Leila");
			rs.insertRow();
		}
	}

	private static void atualizarCorrentistaAPartirCursor() throws SQLException {
		System.out.println("\n ************ atualizarCorrentistaAPartirCursor ************");
		String query = "select * from correntista order by nm asc";
		try (Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
				ResultSet rs = stmt.executeQuery(query)) {
			rs.next();
			rs.updateString("nm", "Manuela");
			rs.updateRow();
		}
	}

	private static void inserirCorrentistaRecuperandoIdGeradoSGBD() throws SQLException {
		System.out.println("\n ************ inserirCorrentistaRecuperandoIdGeradoSGBD ************");
		String sqlInsert = "insert into correntista (nm) values ('Mateus')";
		try (Statement stmt = connection.createStatement()) {
			stmt.executeUpdate(sqlInsert, Statement.RETURN_GENERATED_KEYS);
			ResultSet idRs = stmt.getGeneratedKeys();
			idRs.next();
			Integer id = idRs.getInt(1);
			System.out.println("id gerador para o correntista Mateus=" + id);
		}
	}

	private static void inserirCorrentista() throws SQLException {
		System.out.println("\n ************ inserirCorrentista ************");
		String sqlInsert = "insert into correntista (nm) values ('Joaquim')";
		try (Statement stmt = connection.createStatement()) {
			Integer qtdLinhas = stmt.executeUpdate(sqlInsert);
			System.out.println("qtd linhas: " + qtdLinhas);
		}
	}

	private static void consultarCorrentistasOrdemInvesa() throws SQLException {
		System.out.println("\n ************ consultarCorrentistasOrdemInvesa ************");
		String query = SELECT_CORRENTISTA;
		try (Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
				ResultSet rs = stmt.executeQuery(query);) {
			rs.afterLast();
			while (rs.previous()) {
				String nome = rs.getString("nm");
				System.out.println("Nome=" + nome);
			}
		}
	}

	private static void consultarCorrentistasTryResource() throws SQLException {
		System.out.println("\n ************ consultarCorrentistasTryResource ************");
		String query = SELECT_CORRENTISTA;
		try (Statement stmt = connection.createStatement(); ResultSet rs = stmt.executeQuery(query);) {
			while (rs.next()) {
				String nome = rs.getString("nm");
				System.out.println("Nome=" + nome);
			}
		}
	}

	private static void consultarCorrentistas() throws SQLException {
		System.out.println("\n ************ consultarCorrentistas ************");
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = connection.createStatement();
			String query = SELECT_CORRENTISTA;
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				String nome = rs.getString("nm");
				System.out.println("Nome=" + nome);
			}
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (stmt != null) {
				stmt.close(); // fecha automaticamente o ResultSet!
			}
		}
	}

	private static void conectarSGBD() throws SQLException {
		connection = DriverManager.getConnection(URL, USER, PASSWORD);
	}

	private static void descontarSGBD() throws SQLException {
		connection.close();
	}

}
