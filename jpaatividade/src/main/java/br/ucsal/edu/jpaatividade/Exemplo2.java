package br.ucsal.edu.jpaatividade;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

public class Exemplo2 {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("varejo");
		EntityManager em = emf.createEntityManager();

		em.getTransaction().begin();

		Estado estadoBA = new Estado("BA", "Bahia", new ArrayList<>());
		Cidade cidadeSSA = new Cidade("SSA", "Salvador", estadoBA);
		Cidade cidadeFSA = new Cidade("FSA", "Feira de Santana", estadoBA);
		Cidade cidadeALG = new Cidade("ALG", "Alagoinhas", estadoBA);
		Cidade cidadeCJZ = new Cidade("CJZ", "Cajazeiras", estadoBA);
		estadoBA.getCidades().add(cidadeSSA);
		estadoBA.getCidades().add(cidadeFSA);
		estadoBA.getCidades().add(cidadeALG);
		estadoBA.getCidades().add(cidadeCJZ);

		em.persist(estadoBA);
		em.persist(cidadeSSA);
		em.persist(cidadeFSA);
		em.persist(cidadeCJZ);
		em.persist(cidadeALG);

		em.getTransaction().commit();

		em.clear();

		String oql = "select e from Estado e inner join fetch e.cidades";
		TypedQuery<Estado> query = em.createQuery(oql, Estado.class);
		List<Estado> estados = query.getResultList();

		System.out.println("Pressione ENTER...");
		new Scanner(System.in).nextLine();

		for (Estado estado : estados) {
			System.out.println(estado);
		}

		em.close();
		em = emf.createEntityManager();

		System.out.println(estados.get(0).getCidades());
	}

}
