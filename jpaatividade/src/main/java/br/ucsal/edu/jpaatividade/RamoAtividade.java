package br.ucsal.edu.jpaatividade;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tab_ramo_atividade")
@SequenceGenerator(name = "sq_ramo_atividade", sequenceName = "seq_ramoatividade")
public class RamoAtividade {

	@Id
	@GeneratedValue(generator = "sq_ramo_atividade")
	private Integer id;

	private String nome;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
