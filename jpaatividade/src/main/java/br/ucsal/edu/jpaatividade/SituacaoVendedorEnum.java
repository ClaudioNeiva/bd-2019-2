package br.ucsal.edu.jpaatividade;

public enum SituacaoVendedorEnum {

	SUSPENSO("SPS"), ATIVO("ATV"), AFASTADO("AFS"), FERIAS("FER");

	private String codigoBD;

	private SituacaoVendedorEnum(String codigoBD) {

		this.codigoBD = codigoBD;
	}

	public String getCodigoBD() {
		return codigoBD;
	}

	public static SituacaoVendedorEnum valueOfCodigoBD(String codigoBD) {
		for (SituacaoVendedorEnum situacaoVendedorEnum : values()) {
			if (situacaoVendedorEnum.getCodigoBD().equals(codigoBD)) {
				return situacaoVendedorEnum;
			}
		}
		throw new IllegalArgumentException();
	}

}
