package br.ucsal.edu.jpaatividade;

import java.util.List;

import javax.persistence.FetchType;
import javax.persistence.OneToMany;

public class SituacaoVendedor {

	private Integer id;
	
	private String descricao;
	
	@OneToMany(fetch = FetchType.LAZY)
	private List<Vendedor> vendedores;

	public SituacaoVendedor(Integer id, String descricao, List<Vendedor> vendedores) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.vendedores = vendedores;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Vendedor> getVendedores() {
		return vendedores;
	}

	public void setVendedores(List<Vendedor> vendedores) {
		this.vendedores = vendedores;
	}
	
	
}
