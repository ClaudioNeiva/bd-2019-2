package br.ucsal.edu.jpaatividade;

import javax.persistence.AttributeConverter;

public class SituacaoVendedorConverter implements AttributeConverter<SituacaoVendedorEnum, String> {

	public String convertToDatabaseColumn(SituacaoVendedorEnum attribute) {
		return attribute == null ? null : attribute.getCodigoBD();
	}

	public SituacaoVendedorEnum convertToEntityAttribute(String dbData) {
		return dbData == null ? null : SituacaoVendedorEnum.valueOfCodigoBD(dbData);
	}

}
