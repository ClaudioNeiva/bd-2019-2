package br.ucsal.edu.jpaatividade;

import java.util.List;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tab_vendedor")
public class Vendedor extends Funcionario {

	private Double percentualComissao;

	// @Enumerated(EnumType.STRING)
	@Convert(converter = SituacaoVendedorConverter.class)
	private SituacaoVendedorEnum situacao;

	@ManyToMany(mappedBy = "vendedores")
	private List<PessoaJuridica> clientes;

	public Double getPercentualComissao() {
		return percentualComissao;
	}

	public void setPercentualComissao(Double percentualComissao) {
		this.percentualComissao = percentualComissao;
	}

	public SituacaoVendedorEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoVendedorEnum situacao) {
		this.situacao = situacao;
	}

	public List<PessoaJuridica> getClientes() {
		return clientes;
	}

	public void setClientes(List<PessoaJuridica> clientes) {
		this.clientes = clientes;
	}

}
