package br.ucsal.edu.jpaatividade;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

public class Exemplo1 {

	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("varejo");

	private static EntityManager em = emf.createEntityManager();

	public static void main(String[] args) {
		String queryOQL0 = "select c2  " 
						 + " from Funcionario f inner join f.endereco.cidade c inner join c.estado e inner join e.cidades c2";
		TypedQuery<Cidade> typedQuery1 = em.createQuery(queryOQL0, Cidade.class);
		List<Cidade> cidades = typedQuery1.getResultList();
	}

	private void caju() {
		String querySQL0 = "select c.sigla, c.nome, c.estado_sigla "
				+ " from Estado e inner join Cidade c on (c.estado_sigla = e.sigla) " + " where e.nome = 'caju'";

		String querySQL1 = "select e.sigla, e.nome from estado e "
				+ " where e.sigla in (select c.sigla_estado from cidade c)";

		String queryOQL1 = "select e from Estado e where e.cidades.size > 0";
		TypedQuery<Estado> typedQuery1 = em.createQuery(queryOQL1, Estado.class);
		List<Estado> estados = typedQuery1.getResultList();

		String queryOQL2 = "select new " + Resultado1DTO.class.getCanonicalName()
				+ "(e.sigla, c.sigla) from Estado e inner join e.cidades c ";
		TypedQuery<Resultado1DTO> typedQuery2 = em.createQuery(queryOQL2, Resultado1DTO.class);

		String queryOQL3 = "select new br.ucsal.edu.jpaatividade.Exemplo1.Resultado1DTO(e.sigla, c.sigla) from Estado e inner join e.cidades c ";

		String queryOQLCaju = "select e.sigla, c.sigla from Estado e inner join e.cidades c ";
		Query queryCaju = em.createQuery(queryOQLCaju);
		List<Object[]> result = queryCaju.getResultList();
		for (Object[] row : result) {
			System.out.println(row[0] + "-" + row[1]);
		}
	}

	public class Resultado1DTO {
		private String siglaEstado;
		private String siglaCidade;

		public Resultado1DTO(String siglaEstado, String siglaCidade) {
			super();
			this.siglaEstado = siglaEstado;
			this.siglaCidade = siglaCidade;
		}

		public String getSiglaEstado() {
			return siglaEstado;
		}

		public void setSiglaEstado(String siglaEstado) {
			this.siglaEstado = siglaEstado;
		}

		public String getSiglaCidade() {
			return siglaCidade;
		}

		public void setSiglaCidade(String siglaCidade) {
			this.siglaCidade = siglaCidade;
		}

	}

}
