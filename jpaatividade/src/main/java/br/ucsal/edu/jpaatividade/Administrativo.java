package br.ucsal.edu.jpaatividade;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tab_administrativo")
public class Administrativo extends Funcionario {

	private Integer turno;

	public Integer getTurno() {
		return turno;
	}

	public void setTurno(Integer turno) {
		this.turno = turno;
	}

}
