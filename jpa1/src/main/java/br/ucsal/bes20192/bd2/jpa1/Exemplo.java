package br.ucsal.bes20192.bd2.jpa1;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Exemplo {

	public static void main(String[] args) {

		Aluno aluno = new Aluno(10, "Claudio");

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("aula01jpa");

		EntityManager em = emf.createEntityManager();

		em.getTransaction().begin();

		em.persist(aluno);

		// Outras coisas podem ser feitas aqui
		Aluno aluno2 = em.find(Aluno.class, 10);

		// Aqui vc fa�a um TypedQuery: enviar uma consulta para o SGBD
		// Aqui podem ocorrer erros, por exemplo de trigger!!!!
		// Select -> erro de trigger!!

		// insert into aluno (matricula, nome) values (10, 'Claudio');
		em.flush();

		// Managed - Gerenciado!
		aluno.setNome("Claudio Neiva");

//		em.clear();
//
//		Aluno aluno2 = em.find(Aluno.class, 10);
//
//		System.out.println("aluno2=" + aluno2);

		// update aluno set nome='Claudio Neiva' where matricula = 10;

		em.getTransaction().commit();

		em.close();

		EntityManager em2 = emf.createEntityManager();
		em2.getTransaction().begin();
		aluno.setNome("Claudio Neiva Maria");
		em2.getTransaction().commit();
		em2.close();

		emf.close();

	}

}
