﻿drop table if exists correntista_audit cascade;
drop table if exists correntista cascade;

create table correntista (
	id		serial		not null,
	nm		varchar(100)	not null,
	constraint pk_correntista
		primary key (id));	

create table correntista_audit (
	id_audit	serial		not null,
	dt		timestamp	not null,
	nm_usuario	varchar(100)	not null,
	id_old		integer		,
	nm_old		varchar(100)	,
	id_new		integer		,
	nm_new		varchar(100)	,
	constraint pk_correntista_audit
		primary key (id_audit));	

-- OU

-- create table audit (
-- 	id_audit	serial		not null,
-- 	dt		timestamp	not null,
-- 	nm_usuario	varchar(100)	not null,
-- 	nm_tab		varchar(100)	not null,
-- 	ds		varchar(4000)	not null,
-- 	constraint pk_correntista_audit
-- 		primary key (id_audit));	

drop function if exists f_trg_audit() cascade;

create or replace function f_trg_audit()
returns trigger 
as
$$
begin 
	if TG_OP = 'INSERT' then
		insert into correntista_audit (dt, nm_usuario, id_new, nm_new) 
		values (CURRENT_TIMESTAMP, CURRENT_USER, NEW.id, NEW.nm);
	elsif TG_OP = 'UPDATE' then
		insert into correntista_audit (dt, nm_usuario, id_old, nm_old, id_new, nm_new) 
		values (CURRENT_TIMESTAMP, CURRENT_USER, OLD.id, OLD.nm, NEW.id, NEW.nm);
	elsif TG_OP = 'DELETE' then
		insert into correntista_audit (dt, nm_usuario, id_old, nm_old) 
		values (CURRENT_TIMESTAMP, CURRENT_USER, OLD.id, OLD.nm);
	end if;
	-- Podemos fazer um return null sem afetar as operações realizadas, pois essa função de trigger será utilizar em um
	-- trigger do tipo AFTER. Se fosse um trigger do tipo BEFORE, era necessário utilizar return NEW ou return OLD
	-- a depender do tipo de operação, ou seja , dentro dos ifs anteriores.
	return null;
end;
$$
language PLPGSQL;

drop trigger if exists trg_audit on correntista;

create trigger trg_audit
after insert or delete or update 
on correntista
for each row
execute procedure f_trg_audit();

insert into correntista(nm)
values ('claudio neiva');

update correntista
set nm = 'maria da silva';

delete 
from correntista;

select *
from correntista;

select *
from correntista_audit
order by dt;



