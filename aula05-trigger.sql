﻿-- criando as tabelas de exemplo
drop table if exists conta cascade;
drop table if exists movimento cascade;
drop table if exists saldo cascade;

create table conta (
	cd		serial		not null,
	nm_correntista	varchar(40)	not null,
	constraint pk_conta
		primary key (cd));
		
create table movimento (
	id 		serial		not null,
	cd_conta	integer		not null,
	dt		date		not null,
	vl		numeric(10,2)	not null,
	ds_historico	varchar(100)		,
	constraint pk_movimento
		primary key (id));

create table saldo (
	cd_conta	integer		not null,
	dt		date		not null,
	vl		numeric(10,2)	not null,
	constraint pk_saldo
		primary key (cd_conta, dt));

alter table movimento 
	add constraint fk_movimento_conta
		foreign key (cd_conta)
		references conta;

alter table saldo
	add constraint fk_saldo_conta
		foreign key (cd_conta)
		references conta;

-- criando a função que será disparada pelo trigger

drop function if exists f_trg_ins_upd_movimento();

CREATE FUNCTION f_trg_ins_upd_movimento()
RETURNS TRIGGER
AS
$$
BEGIN
	RAISE NOTICE 'Valor do movimento %, data do movimento %', NEW.vl, NEW.dt;
	if NEW.vl > 1000 then
		RAISE EXCEPTION 'Não é possível movimentar mais de 1000 reais em uma operação.';
	end if;
	RETURN NEW;
END;
$$
LANGUAGE PLPGSQL;

-- criando o trigger que irá dispara a função para os eventos de insert e update (cd_conta, dt e vl)

drop trigger if exists trg_ins_upd_movimento on movimento;

CREATE TRIGGER trg_ins_upd_movimento
BEFORE INSERT OR UPDATE OF cd_conta, dt, vl
ON movimento
FOR EACH ROW
EXECUTE PROCEDURE f_trg_ins_upd_movimento();

-- preencher a tabela de conta com dados de apoio

insert into conta (nm_correntista)
values 
('Claudio Neiva'),
('Maria da Silva');

-- manipular a tabela, provocando o disparo do trigger através do evento de insert na tabela de movimento

insert into movimento (cd_conta, dt, vl, ds_historico)
values
((select cd from conta where nm_correntista = 'Claudio Neiva'), '2019-08-02', 900, 'Salário agosto/2019');

insert into movimento (cd_conta, dt, vl, ds_historico)
values
((select cd from conta where nm_correntista = 'Claudio Neiva'), '2019-09-02', 2000, 'Salário setembro/2019');

-- consultar a tabela para confirmar que o dado não foi inserido

select 	*
from	movimento;
